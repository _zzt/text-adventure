﻿/*

                        텍스트 모험

 이 프로그램은 2016 하제 전기 개발 교육 자료의 과제입니다.
 이 게임은 간단하게 콘솔 텍스트상에서 모험을 하는 게임으로,
 진행하며 적과 마주치거나 물건을 줍는 등의 행동이 주 콘텐츠입니다.

 여러분의 목표는 이 게임 중 구현되지 않은 부분의 코드를 작성해
 이 게임을 멋지게 완성하는 것입니다.
 
 구체적인 지시 사항은 각 빈 부분에 자세히 적혀 있으니 참고하세요.

 이 코드는 아마 지금까지 여러분이 직접 만들었던 것보다 꽤 길고,
 처음 보는 문법도 꽤 많이 있을 것입니다.
 이 코드를 읽고 고쳐 나가면서 많은 것을 배워 나갈 수 있기를 바랍니다.

 2016년 2월 29일 ~ 3월 3일, 원정호 작성

*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Text_Adventure
{
    class Program
    {
        static Random random = new Random(); // 난수를 생성하기 위한 코드입니다.
                                             // new나 Random이 정확히 무슨 의미를 가지는지는 당장 몰라도 됩니다.

        // random.Next(최소, 최대)를 호출하면 최소 이상, 최대 미만의 임의의 정수가 반환됩니다.
        // random.Next(최대)를 호출하면 0 이상, 최대 미만의 임의의 정수가 반환됩니다.

        static string playerName = "지시사항 1 미이수";
        static bool playerDead = false;
        static int playerAttack = 12;
        static int playerHP = 40;
        static int playerMaxHP = 40;
        static int playerKills = 0;

        static int enemyAttack;
        static int enemyHP;
        static int enemyMaxHP;
        static string enemyName;
        static Action enemyUltimate;
        // Action은 '매개변수와 반환값이 없는 함수' 타입입니다.
        /*
            예를 들어 static void Donothing() {} 이 있을 때 enemyUltimate = Donothing; 으로 값을 줄 수 있고
            enemyUltimate();를 사용하면 Donothing();을 호출한 것과 같은 효과가 됩니다.
        */
        static string enemyUltimateName;

        static int progress;
        const int progressMax = 30; // const는 값이 바뀌는 것이 불가능합니다. ("상수"라 부릅니다.) 참고로 const 변수 앞에는 static을 붙이지 않아도 됩니다.

        static void DoNothing() { }

        static void Main(string[] args)
        {
            Intro();

            // 지시사항 1: 주인공의 이름을 물어보고, 그 결과를 playerName에 저장하는 코드를 만드세요.

            //

            int randomValue;


            for (progress = 1; progress <= progressMax; progress++)
            {
                Console.WriteLine(playerName + "은(는) 앞으로 나아갑니다. (진행률 " + progress + "/" + progressMax + ")");
                Thread.Sleep(1500); // Thread.Sleep은 매개변수로 입력한 숫자 * 0.001초 동안 대기합니다.

                // 지시사항 2: 코드 전체에 Console.WriteLine 이후 Thread.Sleep을 부르는 일이 많습니다.
                // 이 두 개의 행동을 깔끔하게 하나의 함수로 만들어서 통합하고, 전체 코드에서 그런 부분을 전부 고쳐 놓읍시다.
                // 함수는 매개변수 2개 (표시할 텍스트의 내용, 이후 대기할 시간)를 가져야 할 것입니다.

                randomValue = random.Next(100);

                if (randomValue < 39)
                {
                    Console.WriteLine("아무것도 마주치지 않았습니다.");
                    Thread.Sleep(1000);
                }
                else if (randomValue < 79)
                {
                    Encounter();
                    // 지시사항 3: Encounter 함수에 있습니다. 가서 보세요.
                    // 지시사항 4: Die 함수에 있습니다.
                    // 적 마주치기

                }
                else if (randomValue < 99)
                {
                    GetItem();
                    // 아이템 줍기
                }
                else
                {
                    // 추가 도전 과제: 1% 확률로 일어나는 뭔가 멋진 걸 만들어보고 싶다면 자유롭게 짜 보세요!
                    // 현재는 아무 일도 일어나지 않은 경우랑 똑같이 처리됩니다.
                    Console.WriteLine("아무것도 마주치지 않았습니다.");
                    Thread.Sleep(1000);
                }

                if (playerDead)  // 플레이어가 죽었다면 여기서 프로그램은 끝나야 합니다.
                    return;

            }
            
            

            Ending();

            // 지시사항 5: 이 코드는 지금까지 가르쳐 준 적이 없는 기술도 몇 가지 씁니다. 구석구석 읽어 보면서 어떤 기술이 쓰였는지 한 번 둘러보세요.

            // 추가 도전 과제: 이 게임을 원하는 만큼 신나게 개조해 보세요! 오류가 발생하지 않는 이상 자유롭게 뭐든지 만져도 좋습니다.
        }

        static void Intro()
        {
            Console.WriteLine("이 게임은 종종 읽을 시간을 주느라 잠깐 동안 멈춰 있게 됩니다.");
            Thread.Sleep(2500);
            Console.WriteLine("게임이 멈춰 있는 동안은 키를 누르지 마세요! 나중에 원치 않는 결정을 하게 될 수도 있습니다.");
            Thread.Sleep(4000);
            Console.Clear(); // 콘솔을 비웁니다.
            Console.WriteLine("그 물을 마시면 영원한 행복을 얻을 수 있다고 알려진 동굴 가장 깊은 곳의 샘.");
            Thread.Sleep(4000);
            Console.WriteLine("어느 젊고 야심찬 모험가 역시 희망을 갖고 굴의 입구로 발걸음을 옮겼습니다...");
            Thread.Sleep(4000);
        }


        // 적과 마주쳤을 때 호출되는 함수입니다.
        static void Encounter()
        {
            int randomValue;
            randomValue = random.Next(7);

            switch (randomValue)
            {
                case 0:
                    SpawnEnemy("트롤", 2, 61, Regenerate, "재생");
                    break;

                case 1:
                    SpawnEnemy("수정 괴물", 4, 35, ShootSpikes, "가시 발사");
                    break;

                case 2:
                    SpawnEnemy("어린 용", 5, 50, FireBreathe, "불 내뿜기");
                    break;

                case 3:
                    SpawnEnemy("허수아비", 0, 40, DoNothing, "가만히 있기");
                    break;

                /*
                지시사항 3: case 4와 5도 구현해서 나만의 색다른 적을 만들어 봅시다.
                case 4:

                    break;

                case 5:

                    break;
                */

                default:
                    SpawnEnemy("하급 요괴", 3, 13, Smite, "강타");
                    break;
            }

            Combat();

        }

        // 적을 만들 때 쓰는 함수입니다.
        static void SpawnEnemy(string name, int attack, int HP, Action ultimate, string ultimateName)
        {
            enemyName = name;
            enemyMaxHP = enemyHP = HP;
            enemyAttack = attack;
            enemyUltimate = ultimate;
            enemyUltimateName = ultimateName;
        }

        //몬스터의 궁극기입니다.
        static void Smite()
        {
            Console.WriteLine("피해량 " + 9 + "!");
            playerHP -= 9;
        }

        static void ShootSpikes()
        {
            Console.WriteLine("피해량 " + 8 + "!");
            playerHP -= 8;
            if (playerHP > 0)
            {
                Console.WriteLine("몸에 박힌 수정이 앞으로의 공격을 막아줄 것 같습니다.");
                Console.WriteLine("최대 및 현재 체력이 영구히 2 오릅니다.");
                playerHP += 2;
                playerMaxHP += 2;
            }
        }

        static void FireBreathe()
        {
            Console.WriteLine("불길이 엄청난 고통을 일으킵니다.");
            Console.WriteLine("남은 체력의 절반인 " + ((playerHP + 1) / 2) + "만큼의 피해!"); // 정수끼리 나눗셈을 하면 그 결과는 나머지를 버린 결과가 됩니다.
            playerHP /= 2;
        }

        static void Regenerate()
        {
            Console.WriteLine("트롤이 자신의 상처를 " + 20 + "만큼 재생합니다.");
            enemyHP += 20;
            if (enemyHP > enemyMaxHP)
                enemyHP = enemyMaxHP;
            Console.WriteLine(enemyName + "의 남은 체력: " + enemyHP);
        }

        // 이 부분에 다른 적의 궁극기도 넣어 봅시다.

           

        //


        // 적과 싸움을 벌일 때 쓰는 함수입니다.
        static void Combat()
        {
            Console.WriteLine("적 " + enemyName + "이(가) 나타났습니다!");
            while (true)
            {
                Console.WriteLine("무엇을 하시겠습니까?");
                Console.WriteLine("A: 공격, S: 도주 (1/3 성공), D: 내 상태 보기, F: 적 상태 보기, Q: 자결");
                Console.WriteLine("(D와 F는 행동을 소모하지 않음)");

                string input = Console.ReadLine();

                input = input.ToLower(); // 이 명령은 input에 있는 모든 대문자를 소문자로 바꿔줍니다. 

                switch (input)
                {
                    case "a":
                        Console.WriteLine(playerName + "은(는) " + enemyName + "을(를) 공격했습니다.");
                        Console.WriteLine("피해량 " + playerAttack + "!");

                        enemyHP -= playerAttack;
                        if (enemyHP < 0)
                            enemyHP = 0;

                        Console.WriteLine(enemyName + "의 남은 체력: " + enemyHP);
                        if (enemyHP == 0)
                        {
                            Console.WriteLine(enemyName + "은(는) 죽었습니다.");
                            return;
                        }
                        break;

                    case "s":
                        if (random.Next(3) == 0)
                        {
                            Console.WriteLine("도주 성공! 적을 따돌리고 앞서나갑니다.");
                            return;
                        }
                        else
                            Console.WriteLine("도주 실패!");

                        break;

                    case "d":
                        Console.WriteLine(playerName);
                        Console.WriteLine("체력: " + playerHP + "/" + playerMaxHP);
                        Console.WriteLine("공격력: " + playerAttack);
                        continue; // continue는 반복의 가장 위로 돌아갑니다. 이 경우 while(true)로 갑니다.
                        break;

                    case "f":
                        Console.WriteLine(enemyName);
                        Console.WriteLine("체력: " + enemyHP + "/" + enemyMaxHP);
                        Console.WriteLine("공격력: " + enemyAttack);
                        Console.WriteLine("궁극기: " + enemyUltimateName);

                        continue;
                        break;

                    case "q":
                        Console.WriteLine(playerName + "은(는) 정말 자결하려고 하나요?");
                        
                        if (AskForYesNo())
                        {
                            Console.WriteLine(playerName + "은(는) 자결을 택했습니다.");
                            Die();
                            return;
                        }
                        Console.WriteLine(playerName + "은(는) 그냥 살기로 했습니다.");
                        break;

                    default:
                        Console.WriteLine("옳은 명령이 아닙니다!");
                        continue;
                        break;
                }

                Thread.Sleep(2000);

                if (random.Next(5) == 0)
                {
                    Console.WriteLine(enemyName + "은(는) 20% 확률의 궁극기 " + enemyUltimateName + "에 성공합니다!");
                    enemyUltimate();
                }
                else
                {
                    Console.WriteLine(enemyName + "은(는) " + playerName + "을(를) 공격했습니다.");
                    Console.WriteLine("피해량 " + enemyAttack + "!");

                    playerHP -= enemyAttack;

                }

                if (playerHP < 0)
                    playerHP = 0;

                Console.WriteLine(playerName + "의 남은 체력: " + playerHP);

                Thread.Sleep(1000);
                if (playerHP == 0)
                {
                    Die();
                    return;
                }

            }
        }

        // 아이템과 마주쳤을 때 호출되는 함수입니다.
        static void GetItem()
        {
            bool isStream = random.Next(2) == 0;

            if (isStream)
            {
                Console.WriteLine("이상한 색깔의 물이 흐르는 물줄기입니다. 마셔 볼까요?");
                if (AskForYesNo())
                {
                    if (random.Next(2) == 0)
                    {
                        Console.WriteLine("체력이 완전히 회복됐습니다.");
                        Thread.Sleep(500);
                        playerHP = playerMaxHP;
                    }
                    else
                    {
                        Console.WriteLine("이런! 이상한 독극물입니다. 최대 체력이 영구히 4 감소했습니다.");
                        Thread.Sleep(500);
                        playerMaxHP -= 4;
                        if (playerHP > playerMaxHP)
                            playerHP = playerMaxHP;
                    }
                }
            }
            else
            {
                Console.WriteLine("이상하게 생긴 보물상자입니다. 열까요?");
                if (AskForYesNo())
                {
                    if (random.Next(2) == 0)
                    {
                        Console.WriteLine("끝내주는 무기를 얻었습니다! 공격력이 영구히 6 올랐습니다.");
                        Thread.Sleep(500);
                        playerAttack += 6;
                    }
                    else
                    {
                        Console.WriteLine("이런! 상자에서 적이 튀어나왔습니다!");
                        Thread.Sleep(500);
                        Encounter();
                    }
                }
            }



        }

        static bool AskForYesNo()
        {
            Console.WriteLine("(Y나 y 외의 답은 전부 '아니오' 처리됩니다.)");
            string input = Console.ReadLine();
            input = input.ToLower();
            return input == "y";
        }

        // 사망 시 호출되는 함수입니다.
        // 지시사항 4: 죽었을 때 죽은 원인을 같이 표시할 수 있도록 함수 구조 및 호출하는 부분을 바꿔 봅시다. 
        // 매개변수를 추가하는 것이 좋을 것 같군요.
        static void Die()
        {
            playerDead = true;
            Console.WriteLine(playerName + "은(는) 결국 죽고 말았습니다....");
            Thread.Sleep(2000);
            Console.WriteLine("진행률: " + progress + "/" + progressMax);
            Console.ReadKey();
        }

        static void Ending()
        {
            Thread.Sleep(1000);
            Console.Clear();
            Console.WriteLine(playerName + "은(는) 마침내 영원한 행복의 샘을 눈앞에 두었습니다.");
            Thread.Sleep(4000);
            Console.WriteLine("샘물은 화사하게 빛나며, 마실 이를 기다리고 있었습니다.");
            Thread.Sleep(4500);
            Console.WriteLine(playerName + "은(는) 영원한 행복을 상상하며, 발걸음을 서서히 앞으로 옮겼습니다....");
            Thread.Sleep(4000);
            Console.WriteLine("게임 클리어!");
            Console.ReadKey();
        }

    }
}
